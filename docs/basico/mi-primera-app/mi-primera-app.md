[TOC]

Nuestra primera aplicación está alojada en el repositorio GitHub oficial de Electron [electron/electron-quick-start](https://github.com/electron/electron-quick-start), se puede clonar el repositorio en nuestra computadora local con un [cliente Git](https://git-scm.com/downloads).

# Descarga del ejemplo 

```
git clone https://github.com/electron/electron-quick-start
```

Yo he añadido el ejemplo a mi repositorio en [/docs/src/basico/mi-primera-app](https://gitlab.com/soka/electron/tree/master/docs/src/basico/mi-primera-app)

# Uso

Accedemos a la carpeta clonada, instalamos las dependencias necesarias y ejectamos la app:

```
cd electron-quick-start
npm install
npm start
```

Si todo va correctamente deberiamos ver una ventana como esta:

![](img/01.png)


# Contenido del proyecto

Nuestra primera App contiene tres archivos principales:

* `main.js`: Script de inicio de la aplicación. Se ocupa de crear la ventana. Ejecuta el proceso principal.
* `package.json`: Apunta al archivo principal de la app `main.js` y enumera las dependencias y detalles de la aplicación.
* `index.html`: Una Web para renderizar con aspecto de ventana de escritorio.

# Descripción de la aplicación

## package.json

Cuando comenzamos de cero este fichero se crea con la instrucción `npm init`.

Un archivo `package.json` básico puede tener algunos elementos típicos como:

```
{
    "name": "tu-app",
    "version": "0.1.0",
	"main": "main.js"
	"scripts": {
		"start": "electron ."
	}
}
```

Fijese que en vez de ejecutar con node definimos el sistema de ejecución electron.

Algunos parámetros a destacar:

* "name": Se refiere evidentemente al nombre de nuestra app. El formato debe ser en minúsculas y con un guion medio de separador de palabras.
* "version": Representa la versión del código fuente del proyecto.
* "description": Breve descripción del cometido de nuestra app.
* "main": Es donde indicamos a Electron donde está el código del proceso principal.
* "scripts": Dice a NPM comandos específicos, en este caso invoca el comando electron. 

Ahora ya podemos ejecutar nuestra aplicación de ejemplo con `npm start`.

## main.js

Fuente: [main.js](../../src/basico/mi-primera-app/main.js).

Todas las aplicaciones en Electron comienzan con un script principal (similar a una app Node.js). El fichero `main.js` crea las ventanas de nuestra aplicación, maneja los eventos y ejecuta el proceso principal. Para mostrar el interface de usuario el proceso principal crea un proceso de renderizado de la ventana usando `BrowserWindow`.

Lo primero es añadir el módulo `electron`:

```
const {app, BrowserWindow} = require('electron')
```

También se puede escribir como sigue:

```
const electron = require('electron');  
const app = electron.app;  
const BrowserWindow = electron.BrowserWindow;  
```

Definimos dos variables constantes:

* `electron.app`: Controla el ciclo de vida de la app.
* `electron.BrowserWindow`: Clase para crear la ventana de la app. Acceso al gestor de ventanas de Chromium.

La siguiente función es la encargada de visualizar la ventana principal usando una instancia de la clase [`BrowserWindow`](https://electronjs.org/docs/api/browser-window) que se asocia a la variable global `mainWindow`. Pasamos al constructor la anchura y altura de la ventana en pixeles. 

Luego llamamos al método `loadFile` indicando la ruta al fichero HTML como parámetro. 

La última instrucción maneja el evento de cierre de la ventana. 

```
function createWindow () {
  mainWindow = new BrowserWindow({width: 800, height: 600})

  mainWindow.loadFile('index.html')

  mainWindow.on('closed', function () {
    mainWindow = null
  })
}
```

El siguiente método espera a que Electron acabe su inicialización antes de llamar a la función que crea la ventana. El evento `ready` es emitido cuando la aplicación ha terminado su iniciación básica. 

```
app.on('ready', createWindow)
```

El evento `window-all-closed` se emitirá cuando todas las ventanas asociadas al app se cierren. Vale decir, cuando no existan instancias de la clase BrowserWindow.

```
app.on('window-all-closed', function () {
    app.quit()
})
```

`activate` es emitido cuando la aplicación está activada. Comprobamos que no este la ventana ya cargada para llamar a la función `createWindow()`.


## index.html

Fuente: [index.html](../../src/basico/mi-primera-app/index.html).

El fichero HTML es cargado usando `BrowserWindow`. Cualquier HTML, CSS o Javascript que funcione en un navegador Web funciona aquí también.

En la página `index.html` definimos el aspecto que le queremos dar. Podemos referenciar a ficheros CSS como [Bootstrap](https://getbootstrap.com/) para darle estilo o a librerias JS como [jQuery](https://jquery.com/).  

```
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Hello World!</title>
  </head>
  <body>
    <h1>Hello World!</h1>
    <!-- All of the Node.js APIs are available in this renderer process. -->
    We are using Node.js <script>document.write(process.versions.node)</script>,
    Chromium <script>document.write(process.versions.chrome)</script>,
    and Electron <script>document.write(process.versions.electron)</script>.

    <script>
      // You can also require other files to run in this process
      require('./renderer.js')
    </script>
  </body>
</html>
```

# Enlaces externos

* [BrowserWindow](https://electronjs.org/docs/api/browser-window).
* [app](https://electronjs.org/docs/api/app#app).
* [Escribiendo tu primera App con Electron](https://electronjs.org/docs/tutorial/first-app): Tutorial en el que se basa este artículo.
* [Creating and Using Windows in Electron](https://coursetro.com/posts/code/120/Creating-and-Using-Windows-in-Electron).
* [Desarrollando aplicaciones de escritorio con Electron.js](https://platzi.com/blog/aplicaciones-escritorio-electron-js/).
* [Creando nuestra primera app de escritorio con HTML, JS y Electron](https://programacion.net/articulo/creando_nuestra_primera_app_de_escritorio_con_html-_js_y_electron_1373).
* [Building a desktop application with Electron](https://medium.com/developers-writing/building-a-desktop-application-with-electron-204203eeb658).
	









