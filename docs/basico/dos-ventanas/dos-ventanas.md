[TOC]

Vamos a hacer un ejemplo algo más avanzado, el siguiente ejemplo abre una nueva ventana cuando pulsamos un boton en la ventana principal.

Fuente en GitLab del proyecto en Electron en [docs/src/basico/dos-ventanas](https://gitlab.com/soka/electron/tree/master/docs/src/basico/dos-ventanas).

Este artículo no sería posible sin en el siguiente tutorial [Creating and Using Windows in Electron](https://coursetro.com/posts/code/120/Creating-and-Using-Windows-in-Electron), no me quiero atribuir los meritos de otras personas. 

# Organizar el código 

Lo primero es aplicar algunos criterios para establecer una organización ahora que tenemos varias ventanas. Hasta el momento nuestra primera aplicación debería tener la siguiente estructura:

```
/node_modules
index.html
main.js
package-lock.json
package.json
```

Vamos a crear una nueva carpeta llamada `/src` y moveremos `index.html` dentro. Cada ventana tendrá su fichero JavaScript asociado, esto significa que crearemos también un fichero `index.js` dentro de la misma carpeta. Para la ventana secundaria crearemos los ficheros `add.js` y `add.html`.

Como hemos movido de sitio `index.html` debemos modificar `/main.js`:

```
mainWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'src/index.html'),
		protocol: 'file:',
		slashes: true
	}))
```
Para usar `url` y `path` debemos añadir las siguientes líneas a la cabecera de `main.js`:

```
const path = require('path')
const url = require('url')
``` 

# Electron-reload

[Electron-reload](https://www.npmjs.com/package/electron-reload) es la forma más sencilla de recargar los contenidos de las ventanas activas [BrowserWindow](https://github.com/electron/electron/blob/master/docs/api/browser-window.md) cuando se modifica el código fuente.

**Instalación**:

```
npm i electron-reload
```

**Uso:**

Lo primero es inicializar el modulo para la ruta de nuestra app, cualquier fichero que cambie provocará el refresco de la app.

```
require('electron-reload')(__dirname);
``` 

# Ventana principal 

Reemplazamos el contenido de `index.html` por el siguiente:

```
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Hello World!</title>
  </head>
  <body>
    
	<button id="notifyBtn">Pulsa el botón</button>
   
    <script src="index.js"></script>
	    
  </body>
</html>
```

El botón ` <button id="notifyBtn">` será el que lance la nueva ventana. 

# Ventana secundaria

Esta será la ventana secundaria, sólo contiene lo indispensable, un enlace que al hacer click sobre el cierra ventana. 

```
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Ventana secundaria</title>
  </head>
  <body>    	
	<a id="closeBtn" href="">Cerrar ventana</a>	
    <script src="add.js"></script>
  </body>
</html>
```

# Abrir una nueva ventana

Definimos los modulos necesarios en la cabecera de `/src/index.js`.

```
const electron = require('electron')
const path = require('path')
const BrowserWindow = electron.remote.BrowserWindow
```

[`BrowserWindow`](https://electronjs.org/docs/api/browser-window) nos permitirá crear una nueva ventana. 

También tenemos que crear una variable para capturar los eventos del botón que hemos creado en `src\index.hml`, usamos el método [`getElementById()`](https://www.w3schools.com/jsref/met_document_getelementbyid.asp) para obtener el elemento HTML basado en su ID.

```
const notifyBtn = document.getElementById('notifyBtn')

notifyBtn.addEventListener('click', function (event) {
  // Acción en respuesta al evento
})
```

Cuando se pulse el botón queremos lanzar una nueva ventana con la plantilla `src\add.html`:

```
const electron = require('electron')
const path = require('path')
const BrowserWindow = electron.remote.BrowserWindow

const notifyBtn = document.getElementById('notifyBtn')

notifyBtn.addEventListener('click', function (event) {
  const modalPath = path.join('file://', __dirname, 'add.html')
  let win = new BrowserWindow({ width: 400, height: 200 })
  win.on('close', function () { win = null })
  win.loadURL(modalPath)
  win.show()
})
``` 

En este momento ya podamos hacer click sobre el botón de la ventana principal que abre una nueva ventana más pequeña encima de la principal.

![](img/01.png)


# Creando una ventana sin marco 

Modificamos el fichero `/src/index.js` para crear una ventana principal sin marcos ni sin menú añadiendo la propiedad `frame: false`.

```
mainWindow = new BrowserWindow({frame: false, width: 800, height: 600})
```

# Ventana secundaria transparente

```
let win = new BrowserWindow({ frame: false, transparent: true, width: 400, height: 200 })
```


# Cerrando la ventana secundaria

Vamos a añadir algo de acción al enlace para cerrar la ventana secundaria en `src/add.js`, localizamos el elemento por el ID `closeBtn`  de la etiqueta HTML usando [`getElementById()`](https://www.w3schools.com/jsref/met_document_getelementbyid.asp), asociamos al elemento un manejador del evento cuando se haga click sobre el. 

La magia opera incluyendo el modulo [`remote`](https://github.com/electron/electron/blob/master/docs/api/remote.md), este provee de mecanismos para comunicar entre procesos (inter-process communication o IPC)) entre el proceso de renderizado (la Web) y el proceso principal, en este caso `add.js`. Usando la llamada `getCurrentWindow()` accedemos a la función que cierra la ventana secundaria. 

Fuente: [`src/add.js`](src/basico/dos-ventanas/src/add.js).

```
const electron = require('electron')
const path = require('path')
const remote = electron.remote

const closeBtn = document.getElementById('closeBtn')

closeBtn.addEventListener('click', function (event) {
	var window = remote.getCurrentWindow();
    window.close();
})
```

# Dandole estilo a las ventanas 


# Enlaces externos

* [Creating and Using Windows in Electron](https://coursetro.com/posts/code/120/Creating-and-Using-Windows-in-Electron). Fuente original de este artículo.
* [How to use live reload in your Electron Project](https://ourcodeworld.com/articles/read/524/how-to-use-live-reload-in-your-electron-project).
* [TWO WAYS TO REACT ON THE ELECTRON ‘CLOSE’ EVENT](http://www.matthiassommer.it/programming/frontend/two-ways-to-react-on-the-electron-close-event/).