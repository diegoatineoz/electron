[TOC]

# ¿Qué es Electron?

[**Electron**](https://electronjs.org/) es un framework basado en [Node.js](https://nodejs.org/es/) (entorno de ejecución para JavaScript construido con el motor de JavaScript V8 de Chrome) que permite programar aplicaciones de ventanas nativas de escritorio basadas en tecnologías Web como Javascript, HTML y CSS. 

**Electron = Node.js + Chromium**

**Electron utiliza páginas web como interfaz gráfica de usuario** por lo que, se podría ver como un navegador Chrome (que internamente posee el motor de Chromium) controlado por javascript.

Electron esta alojado en [GitHub](https://github.com/electron/electron) (lo que garantiza revisiones constantes) accesible de forma pública, es de **código abierto ([ver licencia](https://github.com/electron/electron/blob/master/LICENSE)) y multiplataforma** (funciona bajo Linux, Mac y Windows).

![](img/02.png)

Electron fue desarrollado en 2013 por [Cheng Zhao](https://github.com/zcbenz) ingeniero en GitHub. Inicialmente fue desarrollado para el editor de textos [Atom](https://atom.io/) (con el nombre de Atom Shell) pero desde entonces se ha empleado para crear infinidad de aplicaciones como [GitHub Desktop](https://desktop.github.com/), [Slack](https://electronjs.org/apps/slack), [Whatsapp Desktop](https://www.whatsapp.com/download/) o [Visual Studio Code](https://code.visualstudio.com/), si quieres ver una lista de apps desarrolladas con Electron consulta este enlace [https://electronjs.org/apps](https://electronjs.org/apps).


* ¿Qué es [Chromium](https://www.chromium.org/)? Es la versión de código abierto del navegador Chrome el sistema operativo Chrome OS.
* ¿Qués es [Node.js](https://nodejs.org/es/)? Es un entorno de ejecución de Javascript construido con el motor [JavaScript V8 de Chrome](https://v8.dev/). Fue publicado en 2009 como un proyecto de código abierto. Permite crear aplicaciones del lado del servidor usando JavaScript.

# ¿Como funciona Electron?

Las aplicaciones basadas en Electron se ejecutan en dos procesos, el proceso principal y el proceso de renderizado. Cada uno de estos procesos juega un papel específico. Por ejemplo las APIs del SO estan restringidas al proceso principal. Esto debe ser tenido en cuenta a la hora de diseñar nuestras apps.

![](img/03.png)

El **proceso principal** ("main process") maneja varias actividades a nivel de sistema, como los eventos del ciclo de vida de la aplicación, también es el proceso donde se crean los menús de la aplicación y otros cuadros de dialogo nativos como abrir o salvar un archivo. El proceso principal es el fichero JavaScript referenciado por el fichero packages.json.

El **proceso de renderizado** a su vez tiene otra función, muestra el UI de nuestra app. Cada uno de los procesos de renderizado carga su contenido y lo ejecuta en un hilo separado. Podemos desplegar tantas ventanas como queramos para nuestra app. El proceso de renderizado está aislado de cualquier interacción con los eventos a nivel de sistema, Electron incluye un sistema de comunicación el proceso principal y cualquier proceso de renderizado.

![](img/04.png)

Existen otras alternativas a Electron como por ejemplo [NW.js](https://nwjs.io/).

Actualmente aunque predominan las tecnologías y desarrollos para entornos Web y móviles las aplicaciones de ventanas de escritorio clásicas aún son muy necesarias, por ejemplo cuando queremos interactuar con el sistema operativo local. Gracias a Electron para un desarrollador Web puede ser relativamente fácil crear proyectos de este tipo.  

# Algunas características

* Manejo de temas y uso de componentes de terceros como [Photon](http://photonkit.com/) (aún en desarrollo), [Bootstrap](https://getbootstrap.com/), [Bulma](https://bulma.io/) o [Materialize](https://materializecss.com).
* Incluye un sistema de actualizaciones automáticas. 
* Desarrollar aplicaciones multiplataforma.
* Reportes cuando la app no responde (appCrash).
* Herramientas para depuración y análisis de código y testing.
* Tambien poder integrar a bases de datos relacionales o NoSQL.
* Con la herramienta  ‘electron-packager’ podemos empaquetar la app para destribuirla. 

# Enlaces externos

* tutorialspoint [How Electron Works](https://www.tutorialspoint.com/electron/how_electron_works.htm).
* openwebinars.net [Ventajas de usar Electron](https://openwebinars.net/blog/ventajas-de-usar-electron/).
* TransSolutions [Electron – Un framework para desarrollo de aplicaciones de escritorio multiplataforma (Primera Parte)](http://www.tss.com.pe/blog/electron-un-framework-para-desarrollo-de-aplicaciones-de-escritorio-multiplataforma-primera-parte).





